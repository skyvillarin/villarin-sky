console.log("Hello World");

// Arithmetic Operators
	//+,-,*,/,%
	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	//difference
	let difference = x - y;
	console.log("Result of difference operator: " + difference);

	//product
	let product = x * y;
	console.log("Result of product operator: " + product);

	//quotient
	let quotient = x / y;
	console.log("Result of quotient operator: " + quotient);

	//remainder (modulo)
	let remainder = y % x;
	console.log("Result of remainder operator: " + remainder);

	let a = 10;
	let	b = 5;

	let remainderA = a % b;
	console.log(remainderA);//0

//Assignment Operator
	//Basic Assignment operator (=)
	let assignmentNumber = 8;

	//Addition Assignment Operator (+=)

	assignmentNumber = assignmentNumber + 2;
	assignmentNumber += 2;
	console.log(assignmentNumber);

	//Subtraction/Multiplication/Division Assignment Operator
	assignmentNumber -= 2;
	console.log("Result of -= : " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of *= : " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of -= : " + assignmentNumber);

	//Multiple Operators and Parentheses

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log(mdas);
	/* 	1. 3 * 4 = 12
		2. 12 / 5 = 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6 */

	let pemdas = 1 + (2-3) * (4/5);
	console.log(pemdas);
	/* 	1. 4/5 = 0.8
		2. 2-3 = -1
		3. -1 * 0.8 = -0.8
		4. 1 + -0.8 = 0.2 */

	//Increment and Decrement
		//operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

	let z = 1;
		let increment = ++z;
		console.log("Result of pre-increment: " + increment);
		console.log("Result of pre-increment: " + z);
		//the value of z is added by a value of 1 before returning the value and storing it in the variable "increment"
		//increase then reassign is observed here

	increment = z++;
		console.log("Result of post-increment: " + increment);
		console.log("Result of post-increment: " + z);
		//the value of z is retruned and stored in the variable "increment" then the value of z is increased by one
		//reassign then increase is observed here

		let decrement = --z;
		console.log("Result of post-decrement: " + increment);
		console.log("Result of post-decrement: " + z);

		decrement = z--;
		console.log("Result of post-decrement: " + increment); // result 2
		console.log("Result of post-decrement: " + z); // result 1

	//Type Coercion
		//the automatic or implicit conversion of values from one data type to another

		let numA = '10';
		let numB = 12;

		let coercion = numA + numB;
		console.log(coercion);//result "1012"
		console.log(typeof coercion);

		let numC = 16;
		let numD = 14;
		let nonCoercion = numC + numD;
		console.log(nonCoercion); //result "30"
		console.log(typeof nonCoercion);

		let numE = true + 1; //boolean true = 1
		console.log(numE);

		let numF = false + 1; //boolean false = 0
		console.log(numF);

	//Comparison Operators
		let juan = 'juan';

	//Equality Operator
		console.log (1 == 1); //true
		console.log (1 == 2); //false
		console.log (1 == '1'); //true
		console.log (0 == false); //true
		console.log ('juan' == 'juan'); //true
		console.log ('juan' == juan); //true

	//Inequality Operator
		console.log (1 != 1); //false
		console.log (1 != 2); //true
		console.log (1 != '1'); //false
		console.log (0 != false); //false
		console.log ('juan' != 'juan'); //false
		console.log ('juan' != juan); //false

	//Strict Equality Operator
		console.log('===');
		console.log (1 === 1); //true
		console.log (1 === 2); //false
		console.log (1 === '1'); //false
		console.log (0 === false); //false
		console.log ('juan' === 'juan'); //true
		console.log ('juan' === juan); //true

	//Inequality Operator
		console.log('!==');
		console.log (1 !== 1); //false
		console.log (1 !== 2); //true
		console.log (1 !== '1'); //true
		console.log (0 !== false); //true
		console.log ('juan' !== 'juan'); //false
		console.log ('juan' !== juan); //false

	//Relational Operators
		let abc = 50;
		let def = 65;

		let isGreaterThan = abc > def; 
		let isLessThan = abc < def; 
		let isGTOrEqueal = abc >= def; 
		let isLTOrEqueal = abc <= def; 

		console.log(isGreaterThan); //false
		console.log(isLessThan); //true
		console.log(isGTOrEqueal); //false
		console.log(isLTOrEqueal); //true

		let numStr = "30";
		console.log(abc > numStr);//true
		console.log(def <= numStr);//false
		//forced coercion changed the string "30" to a number

		let str = "twenty";
		console.log(def >= str); //false
		//javascript cannot evaluate the string "twenty" to a number

	//Logical Operators
		let isLegalAge = true;
		let isRegistered = false;

	//&& (AND), || (OR), ! (NOT)
	

	//&&
	//Return true if all operands are true

		let allRequirementsMet = isLegalAge && isRegistered;
		console.log("Result of AND operator: " + allRequirementsMet);//result false

	//||
	//Returns true if ONE of the operands are true
		let someRequirementsMet = isLegalAge || isRegistered;
		console.log("Result of OR operator: " + someRequirementsMet);//result true

	//!
	//Returns the opposite value
		let someRequirementNotMet = !isRegistered
		console.log("Result of NOT operator: " + someRequirementNotMet);//
