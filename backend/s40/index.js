/*
	npm init - used to initialize a new package.json file
	package.json - contains info about the project, dependencies, and other settings
	package-lock.json - locks the versions of all installed dependencies to its specific version
*/
const express = require("express")
const app = express();
const port = 3000; 
//Middlewares
	//a software that provides common services and capabilities to application outside of what's offered by operating syste,
//app.use() is a method used to run another function or method for our expressjs api. It is used to run middlewares

//allows our app to read json data
app.use(express.json())
//allows our app to read data from forms. This also allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}))

app.get("/",(req,res)=>{
	res.send("Hello World")
})

app.get("/hello",(req,res)=>{
	res.send("Hello from /hello endpoint")
})

/*app.post("/hello",(req,res)=>{
	res.send("Hello from the post route")
})*/

app.post("/hello",(req,res)=>{
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})

//MA1
//Refactor the post route to receive data from the req.body.?
//Send a message that has the data received

	//`Hello firstName lastName`

//MA2
	//Create a route for a put method request on the "/" endpoint
	//Send a message as a response: "Hello from the ExpressJS PUT method route!"

	//Create a route for a delete method request on the "/" endpoint
	//Send a message as a response: "Hello from the ExpressJS DELETE method route!"

app.put('/',(req,res)=>{
	res.send("Hello from a put method route!")
})

app.delete('/',(req,res)=>{
	res.send("Hello from a delete method route!")
})
//array will store objects when the '/signup' route is accessed
let users = []


/*app.post("/signup",(req,res)=>{
	
	console.log(req.body)

	users.push(req.body)
	res.send(users)

})*/

app.post("/signup",(req,res)=>{
	
	console.log(req.body)


	if(req.body.username !== '' && req.body.password !== ''){

		users.push(req.body)
		res.send(`User ${req.body.username} has been successfully registered`)

	}else{
		res.send(`Please input BOTH username and password`)
	}

})

//MA3 (5 min. 8:02PM)
	//refactor the signup route
		//If contents of the req.body with the property of username and password is not an empty string, this will store the user object via Postman to the users array
			//send a response back to the client/Postman after the request has been processed
		//Else, if the username and password are not complete, an error message will be sent back to the client/Postman
			//please input both username and password

app.put("/change-password",(req,res)=>{

	let message;

	for(let i=0; i<users.length; i++){

		if(req.body.username == users[i].username){

			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated!`

			console.log(users);

			break;
		}else {
			message = "User does not exist"
		}

	}
	
	res.send(message);

})

//Activity

//Create a GET route that will access the /home route that will send a response with a simple message: "Welcome to the home page"

app.get("/home",(req,res)=>{
	res.send("Welcome to the home page")
})

// Create a GET route that will access the /users route that will send a the users array as a response.
app.get("/users",(req,res)=>{
	res.send("the users array")
})

//Create a DELETE route that will access the /delete-user route to remove a user from the mock database.

// The delete-user route logic is as follows:
	//Create a variable to store the message to be sent back to the client/Postman 
	//Create a condition to check if there are users found in the array
		//If there is, create a for loop that will loop through the elements of the "users" array
	//Add an if statement that if the username provided in the request body and the username of the current object in the loop is the same
		//If it is, remove the current object from the array. You can use splice array method.
	//Change the message to be sent back by the response: ”User <username> has been deleted.”
	//Break the loop.

	//Outside the loop, add an if statement that if the message variable is undefined, update the message variable with the following message: "User does not exist."
	//Add an else statement if there are no users in the array, update the message variable with the following message: "No users found."
	//Send a mess age variable as a response back to the client/Postman once the user has been deleted or if a user is not found



app.delete("/delete-user", (req, res) => {
  let message;
  let userFound = false;

  for (let i = 0; i < users.length; i++) {
    if (req.body.username == users[i].username) {
      users.splice(i, 1);
      userFound = true;
      message = `User ${req.body.username} has been deleted.`;
      break;
    }
  }

  if (userFound) {
    res.send(message);
  } else {
    if (users.length === 0) {
      message = "No users found.";
    } else {
      message = "User does not exist!";
    }
    res.send(message);
  }
});

//End of Activity


if(require.main === module){
	app.listen(port,()=>console.log(`Server running at port ${port}`))
}

module.exports = app;