let http = require("http");

// We use the require directive to include and laod Node.js modules
	// modules - objects that contains pre-built codes already
				
//http - default module that comes from NodeJS. Allows us to transfer data using HTTP and use methods that let us create servers
	//http module lets Node.js transfer datea using Hyper text Transfer Protocol (HTTP)
		//HTTP allows fetching of resources such as HTML documents
			//protocol to client-server communication
				//http://localhost:4000 - server/application

// What is a CLIENT?
	// an application which creates requests for resources from a server
	// will trigger an action, in the web development context, through a URL and wait for the response of the server

// What is a SERVER?
	// able to host and deliver resources requested by a client

// What is NODE.JS?
	// runtime environment that allows us to create/develop backend/server-side applications with JS

http.createServer(function(request,response){
	response.writeHead(200,{'Content-type':'text/plain'})

	// createServer() method - allows us the handle requests and response from a client and server respectively
		// receives 2 objects: request (receives first) and response

	// response.writeHead() is a method of the response object that allows us to add headers to our response
		// HTTP Status Code:
			// 200 - ok
			// 404 - resource can not be found
		// 'Content-type' - pertains to data type of our response

	response.end('Hi, my name is Sky!')

	// response.end

}).listen(4000)
	// .listen() - allows us to assign a port to our server. This will allow us to serve our index.js server in the our local machine assigned to port 4000
		// usual ports for web development: 4000, 4040, 8000, 5000, 3000, 4200

console.log('Server is running at localhost:4000')