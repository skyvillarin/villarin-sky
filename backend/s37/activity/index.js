//Add solution here

const http = require('http'); 
const port = 3000;

const app = http.createServer((request,response)=>{

	if(request.url == '/login'){
		response.writeHead(200,{'Content-type':'text/plain'})
		response.end('You are in the login page')
	}
	else {
  		response.writeHead(404, {'Content-type':'text/html'})
  		response.end(`
      <!DOCTYPE html>
      <html>
        <head>
          <title>404 Not Found</title>
        </head>
        <body>
          <h1>404 Not Found</h1>
          <p>Page not available</p>
        </body>
      </html>
    `)
	}

})
app.listen(port);
console.log(`Server is successfully running at localhost: ${port}`)





















//Do not modify
module.exports = {app}