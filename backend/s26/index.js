console.log("Array Traversal!")
//Create a variable that will store an array of at least 5 of your daily routine in the weekends.
//Create a variable which will store at least 4 capital cities in the world
//Log variables in the console and send ss in hangouts

let tasks = ["Sleeping","Meal Prep","Netflix","Eating out","Work"];
let capitalCities = ["Paris","Bangkok","Tokyo","Madrid"];
console.log(tasks);
console.log(capitalCities);



let task1 = "Brush Teeth";
let task2 = "Eat Breakfast";
let hobbies = ["Play League","Read a book","Listen to music","Code"];
console.log(typeof hobbies);
//Arrays - special type of object, key value pair is indexnumber:element
	// make it easy to manage and manipulate a set of data
	// another term for functions associated with an object and is used to execute statements that are relevant to a specific object

	//Examples
	let grades =[98.5, 94.6, 90.8, 88.9];
	let computerBrands = ['Acer','Asus','Lenovo','HP','Neo','Redfox','Gateway','Toshiba','Fujitsu'];
	let mixedArr = [12,'Asus',null,undefined,{}];
	let arraySample = ['Cardo','One Punch Man',25000,false];
	console.log(grades);
	console.log(computerBrands);
	console.log(mixedArr);
	console.log(arraySample);
	//index numbers start at 0 and is included in the count
	//recommended to store multiple related values in an array, not random

	//Alternative way to write arrays
		let myTasks = [
			'drink html',
			'eat javascript',
			'inhale css',
			'bake react'
			];

		//Array with values from variables:
		let username1 = 'gdragon22';
		let username2 = 'gdino21';
		let username3 = 'ladiesman217';
		let username4 = 'transformer45';
		let username5 = 'noobmaster68';
		let username6 = 'gbutiki78';

		let guildMembers = [username1,username2,username3,username4,username5,username6];
		console.log(guildMembers);
		console.log(myTasks);

	//[.length property]
		// allows us to get and set the total number of items or elements in an array
		// gives a number data type

		console.log(myTasks.length); // result 4
		console.log(capitalCities.length); // result 4

		let blankArr = [];
		console.log(blankArr.length); // result 0

		let fullName = "Cardo Dalisay";
		console.log(fullName.length); // result 13

		// can also set the total number of items in an array
		myTasks.length = myTasks.length-1; 
		console.log(myTasks.length); // result 3
		// logging in myTask in console will show 3 items since the 4th one will be deleted
		// to delete a specific item in an array, we can employ array methods

		// using decrementation
		capitalCities.length--;
		console.log(capitalCities); // result 4th element is deleted

		// cannot do the same with a string
		fullName.length = fullName.length-1
		console.log(fullName.length); // result 13 still

		let theBeatles = ["John","Paul","Ringo","George"];
		theBeatles.length++
		console.log(theBeatles);// result will show empty for the last element, sometimes undefined

		theBeatles[4] = fullName;
		console.log(theBeatles); // cardo dalisay will be added to theBeatles making it 5 members

		theBeatles[theBeatles.length] = "Tanggol";
		console.log(theBeatles); // result Tanggol will be added to the array

	// [Read from Arrays]
		// access elements with the use of their index
		/* Syntax:
			arrayName[indexnumber]
		*/

		console.log(capitalCities[0]); // result will show first element which is Paris

		console.log(grades[100]); // result undefined, no 100 elements

		let lakersLegends = ["Kobe","Shaq","LeBron","Magic","Kareem"];
		console.log(lakersLegends[2]); // result 3rd element LeBron
		console.log(lakersLegends[4]); // result 5th element Kareem

		// can also save array items in another variable
		let currentLaker = lakersLegends[2];
		console.log(currentLaker); // LeBron will be saved as currentLaker

		console.log("Array before assignment")
		console.log(lakersLegends);
		lakersLegends[2] = "Pau Gasol";
		console.log("Array after reassignment")
		console.log(lakersLegends); // result will show array replacing 3rd element LeBron to Pau Gasol

		//mini activity
			//update/reassign the last 2 items in the array with your own personal faves. log the favoriteFoods array in the console with its updated elements using their index number

			let favoriteFoods = ["Tonkatsu","Adobo","Pizza","Lasagna","Sinigang"];
			console.log(favoriteFoods);
			favoriteFoods[3] = "California Maki"
			favoriteFoods[4] = "Lechon"
			console.log(favoriteFoods); // result lasagna and sinigang will be replaced with cali maki and lechon

			//create a function named findBlackMamba which is able to receive an index number as a single argument and return the item accessed by its index
				//function should be able to receive one argument
				//return the blackMamba accessed by the index
				//create a variable outside the function called blackMamba and store the value returned by the function in it
				//log the blackMamba variable (Kobe) in the console

			function findBlackMamba(index){
				return lakersLegends[index]
			}
			let blackMamba = findBlackMamba(0);
			console.log(blackMamba);// result Kobe

			//create a function called addTrainers that can receive a single argument, add the argument in the end of theTrainers arrays, invoke and add an argument to be passed in the function, log the trainer's array in console, send ss
				//trainers to be added brock misty

			let theTrainers = ["Ash"];
			function addTrainers(trainer){
				theTrainers[theTrainers.length] = trainer;
			}
			addTrainers("Misty");
			addTrainers("Brock");
			console.log(theTrainers); // misty and brock will be added


	// Access the last element of an array
		// since first element is 0, subtracting 1 to the length of the array will offset the value by one allowing us to get the last element

		let bullsLegends = ["Jordan","Pippen","Rodman","Rose", "Kukoc"];
		let lastElementIndex = bullsLegends.length-1; // since length is 5 (5 elements), 5-1 is 4, [4] in the array is kukoc since [0] is jordan
		console.log(bullsLegends[lastElementIndex]); // result kukoc


	//Add items into the array
		let newArr = [];
		console.log(newArr[0]);

		newArr[0] = "Cloud Strife";
		console.log(newArr);

		newArr[1] = "Tifa Lockhart";
		console.log(newArr); // result cloud strife and tifa lockhart

		newArr[newArr.length] = "Barret Wallace";
		console.log(newArr); // barret wallace joins the newArr

		newArr[newArr.length-1] = "Aerith Gainsborough";
		console.log(newArr); // replaces barret


	// Loop over an array
		for(let index = 0; index<newArr.length; index++){
			console.log(newArr[index])
		}

		let numArr = [5,12,30,48,40];
		for(let index=0; index<numArr.length; index++){
			if(numArr[index] % 5 === 0){
				console.log(numArr[index] + " is dvisible by 5!")
			}
			else {
				console.log(numArr[index] + " is not divisible by 5!")
			}
		}


	// Multi dimensional array 
			//array in an array
			//stores complex data structures
		let chessBoard = [
		    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
		    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
		    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
		    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
		    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
		    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
		]; 
		console.log(chessBoard);

		// accessing elements in multidimensional array
		console.log(chessBoard[1][4]); // result e2
		console.log("Pawn moves to: " + chessBoard[1][5]); // result f2


	