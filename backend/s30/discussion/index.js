console.log('ES6 Updates!')

//ES6 Updates
	//ES6 is one of the latest versions of writing JS and in fact ONE of the MAJOR updates

	//let and const
		// new standards of creating variables
		// hoisting allows us to use functions and variables before declared 
			// might cause confusion so best to avoid using variables before they are declared

		
		console.log(varSample); // undefined
		var varSample = "Hoist me up!";

		// if you have used nameVar in other parts of the code, you might be surprised at the output we might get
		var nameVar = "Camille";
		if(true){
			var nameVar = "Hi!"
		}

		var nameVar = "C"

		console.log(nameVar); // result C

		let name1 = "Cee"
		if (true){
			let name1 = "Hello";
		}
		console.log(name1); // Result is Cee since it's declared as global variable via let, lesser errors instead of using var


	//[**] - Exponential operator
		// latest
		const firstNum = 8**2;
		console.log(firstNum); // result 64

		// before
		const secondNum = Math.pow(8,2);
		console.log(secondNum); // result 64

	// Mini activity
			/* Create a new variable called concatSentence1
				Concatenate and save a resulting string into concatSentence1
				Log the concatSentence1 in your console and take a screenshot
				the sentence must have spaces and punctuation*/
		let string1 = "fun";
		let string2 = "bootcamp";
		let string3 = "Coding";
		let string4 = "JavaScript";
		let string5 = "Zuitt";
		let string6 = "love";
		let string7 = "Learning";
		let string8 = "I";
		let string9 = "is";
		let string10 = "in"

		// long version
		let concatSentence1 = string3 + " " + string9 + " " + string1 + "!" ;
		console.log(concatSentence1);

		// other version using template literals
		let concatSentence2 = `${string3} ${string9} ${string1}!`;
		console.log(concatSentence2);
			// no need to use + operation to add spaces


	//${} - placeholder that is used to embed jS expressions when creating strings using template literals ``
		let name = "Cyclops";
		let message = `Hello ${name}! Welcome to programming!`;
		console.log(`Message with template literals: ${message}`);

		const anotherMessage = `
			${name} attended a math competition. 

			He won it by solving the problem 8**2 wiht the solution of ${firstNum}!
		`
		console.log(anotherMessage); // result will show multi line breaks

		const interestRate = .1;
		const principal = 1000;
		console.log(`The interest on your savings account is: ${principal * interestRate}`)
		// can use operators or computations in template literals and ${}

		let dev = {
			name: "Peter",
			lastName: "Parker",
			occupation: "Web developer",
			income: 50000,
			expenses: 60000
		};
		console.log(`${dev.name} is a ${dev.occupation}.`);
		console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income-dev.expenses}`)
				// can be applied in objects combined with strings and computations

	// Array destructuring
		// allows us to unpack elements in arraysinto distinct variables, name array elements with variables (instead of using index numbers), and helps with code readiblity

		//index number way
		const fullName = ["Juan","Dela","Cruz"];
		// pre-array destructuring
		console.log(fullName[0]);
		console.log(fullName[1]);
		console.log(fullName[2]);

		console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);

		// array destructuring way
		const [firstName,middleName,lastName] = fullName;
		console.log(firstName);
		console.log(middleName);
		console.log(lastName);
		console.log(`Hello ${firstName} ${middleName} ${lastName}! I was enchanted to meet you!`)
			// more efficient, variables are more specific

		// array destructuring example 2
		let kupunanNiEugene = ["Eugene","Alfred","Vincent","Dennis","Taguro","Master Jeremiah"];
		let [kupunan1, kupunan2, kupunan3, kupunan4, ,kupunan5] = kupunanNiEugene
		console.log(`${kupunan1} ${kupunan2} ${kupunan3} ${kupunan4} ${kupunan5}`) 
				// Taguro is not shown since he was not assigned a variable in the 2nd array (blank space)

	// Object destructuring
		// allows us to unpack properties of objects into distinct variables and shortens the syntax for accessing properties from objects 

		const person = {
			givenName: "Jane",
			maidenName: "Dela",
			familyName: "Cruz"
		}
		//pre-object destructuring
		console.log(person.givenName);
		console.log(person.maidenName);
		console.log(person.familyName);
		console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`)

		//object destructuring way
		const {n1, n2, n3} = person;
		console.log(n1);
		console.log(n2);
		console.log(n3);
		console.log(`Hello ${n1} ${n2} ${n3}! It's good to see you again!`) // result undefined, must use property name unlike arrays

			// proper way
			const {givenName, maidenName, familyName} = person;
			console.log(givenName);
			console.log(maidenName);
			console.log(familyName);
			console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!`) 


		function getFullName ({givenNames, maidenNames, familyNames}){
			console.log(`${givenName} ${maidenName} ${familyName}`)
		}
		getFullName(person);
		// even if there is an s in givenName after function, this will still work since we have established a parameter already 


	// Arrow functions
		// Compact alternative syntax to traditional functions
		// useful for code snippets where creating fx will not be reused in any other portion of the code
		// adhere to DRY (Don't Repeat Yourself) principle where there's no longer a need to create a function and think of a name for functions that will only be used in certain code snippets

		function displayMsg(){
			console.log("Hi");
		}
		displayMsg();
			// normal convention with function name

		let displayHello = () => {
			console.log("Hello World!")
		}
		displayHello();
			// func expression - object within a function

		//converting a regular function naming convention to arrow functions
			// normal
			/*function printFullName (firstName,middleInitial,lastName){
				console.log(`${firstName} ${middleInitial} ${lastName}`);
			}
			printFullName("John","D","Smith");*/

			//arrow version
			let printFullName = (firstName,middleInitial,lastName) => {
				console.log(`${firstName} ${middleInitial} ${lastName}`);
			}
			printFullName('John',"D",'Smith');


			// arrow function with loops
				// pre-arrow way
				const students = ["John","Jane","Natalia","Jobert","Joe"]
				students.forEach(function(student){
					console.log(`${student} is a student!`);
				})
					//parameter for foreach is always the singular version of the array name

				// arrow version
				students.forEach((student)=>{
					console.log(`${student} is a student. (from arrow)`);
				})
					// no more function keyword since the arrow coming from the parentheses and a code block after indicates a function


	//Implicit Return Statement
		/* There are instances when you can omit the "return" statement.
			This works because even without the return statement, JS implicitly adds it for the result of the function */

		// normal way
			/*
			function add (x,y){
				return x + y;
			}

			let total = add(1,2);
			console.log(total); */

		// arrow function version
			/*
			const add = (x,y) =>{
			return x + y;
			}
			let total = add(1,2);
			console.log(total); */

		// arrow but without the return and {}
			const add = (x,y) => x + y;
			let total = add(1,2);
			console.log(total);


	// Default Function Argument Value
		const greet = (name = 'User') =>{
			return `Good morning, ${name}!`
		} 
		console.log(greet()); // result if (name) only is undefined, no argument yet, adding = 'User' after (name) will show User
		console.log(greet("John"));


	// Class-based Object Blueprints
		/* Allows creation/instantiation of objects using classes as blueprints
		*/

	// Creating a class
		/* The constructor is a special method of a class for creating/initializing an object for that class
			this is used to access the objects property created in a class, allowing it to be reassigned with a value
		*/

		class Car {
			constructor(brand,name,year){
				this.brand = brand;
				this.name = name;
				this.year = year;
			}
		}

		// create an instance using "new"
		let myCar = new Car();
		console.log(myCar);

		myCar.brand = "Ford";
		myCar.name = "Ranger Raptor";
		myCar.year = 2021;
		console.log(myCar);

			// can also be
			const myNewCar = new Car("Toyota","Vios",2021);
			console.log(myNewCar);


	// Traditional functions vs Arrow Functions as methods 

		let character1 = {
			name: "Cloud Strife",
			occupation: "Soldier",
			greet: function(){
				// In a traditional function, this keyword refers to the current object where the method is 
				console.log(`Hi! I'm ${this.name}`);
			}
		}

		character1.greet();

		// Arrow function version of method

		let character2 = {
			name: "Cloud Strife",
			occupation: "Soldier",
			greet: () => {
			
				console.log(this);
				console.log(`Hi! I'm ${this.name}`);
			}
		}

		character2.greet();
			// result will show a global window object (container of global variables, functions, objects. Effect of using arrow inside a method)


		// let character3 = {
		// 	name: "Cloud Strife",
		// 	occupation: "Soldier",
		// 	greet: () => {
			
		// 		console.log(this);
		// 		console.log(`Hi! I'm ${this.name}`);
		// 	}
		// 	introduceJob: function(){
		// 		console.log(`Hi! I'm ${this.name}. I'm a ${this.occupation}`);
		// 	}
		// }

		// character3.greet();

		// mini activity

	class Character {
			constructor(name,role,strength,weakness){
				this.name = name;
				this.role = role;
				this.strength = strength;
				this.weakness = weakness;
				this.introduce = () =>{
					console.log(`Hi! I am ${this.name}!`)
				}
			}
		}

		const starlightAnya = new Character("Starlight Anya","Mage of Cuteness","Mama and Papa","Peanuts");
		console.log(starlightAnya);
		starlightAnya.introduce();
