// Setup basic Express JS server
	//Dependencies
	const express = require("express");
	const mongoose = require("mongoose");
	const taskRoute = require("./routes/taskRoute"); //import all routes

	//Server setup
	const app = express();
	const port = 4000;

	//Middlewares
	app.use(express.json());
	app.use(express.urlencoded({extended:true}))

	//Database connection (connect MongoDB Atlas) - add connection string to specific db
	mongoose.connect("mongodb+srv://skyvillarin:admin123@batch-297.tj9j1s5.mongodb.net/taskDB?retryWrites=true&w=majority",{
		useNewUrlParser : true,
		useUnifiedTopology : true
	});
	let db = mongoose.connection;
	db.on("error",console.error.bind(console,"connection error"))
	db.once("open",()=>console.log("We're connected to the cloud database"))

	app.use("/tasks", taskRoute);

	//Server listening
	if(require.main === module){
		app.listen(port,()=>console.log(`Server running at port ${port}`));
	}
	module.exports = app;