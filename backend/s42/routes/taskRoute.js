// Contain all the endpoints for our application
const express = require("express");

//Create a Router instance that functions as a middleware and routing system
const router = express.Router()

//Use functions inside taskController.js
const taskController = require("../controllers/taskController")

//[Routes]
	// responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
	// all the business logic is done in the controller
	router.get("/",(req,res)=>{
		taskController.getAllTasks().then(resultFromController=>res.send(resultFromController))
	})

	router.post("/",(req,res)=>{
		taskController.createTask(req.body).then(resultFromController=>res.send(resultFromController))
	})

	// Route to delete a task
		// this route expects to recceive a DELETE request at the "/tasks/:id"
		// task id is obtained from the URL is denoted by ":id" identifier in the route, the ":id" will become a wildcard
		// the id to be copied in postman is the alphanumeric id from a task in the get tasks 
	router.delete("/:id",(req,res)=>{
		taskController.deleteTask(req.params.id).then(resultFromController =>res.send(resultFromController))
	})

	// Route to update a task
	// This route expects to receive a PUT request at the "/tasks/:id"
	router.put("/:id",(req,res)=>{
		taskController.updateTask(req.params.id, req.body).then(resultFromController =>res.send(resultFromController))
	})

	//Group activity

	//Route to mark task as complete
	router.put("/:id/complete", (req, res) => {
  taskController
    .completeTask(req.params.id)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router; // use module.exports to export the router object to use in index.js
