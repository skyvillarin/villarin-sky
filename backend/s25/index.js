console.log("JS Objects!");

//an object is a data type that is used to represent real world objects
//info stored in objects are represented in a "key:value" pair
//key - property of an object, diff data types can be stored here creating much complex data structures
/*Syntax: 
	let objectName = {
		keyA: valueA,
		keyB: valueB
	}
*/

	let ninja = {
		name: "Naruto",
		village: "Konoha",
		occupation:"Hokage"
	}
	console.log("Result from creating objects using initializers/literal notation");
	console.log(ninja)
	console.log(typeof ninja);

	let dog = {
		name: "Whitey",
		color: "white",
		breed: "Chihuahua"
	}

//Objects using contstructor function - uses capital letters
	//creates a reusable function to create several objects that have the same data structure
	/* Syntax:
		function ObjectName(keyA,keyB){
			this.keyA = keyA;
			this.keyB = keyB;
		}
		
	*/

	//constructor 
	function Laptop(name,manufactureDate){
		this.name = name; // this keyword allows us to assign a new object's properties by associatin them with the values received from a constructor function's paramater
		this.manufactureDate = manufactureDate;
	}

	//instance - a concrete occurence of any object which emphasizes on its distinct/unique identity
	//newly created objects
	let laptop1 = new Laptop('Lenovo', 2022) // new creates an instance of an object
	console.log('Result from creating objects using object constructor');
	console.log(laptop1)

	let myLaptop = new Laptop('Macbook Air', 2020);
	console.log('Result form creating objects using object constructor');
	console.log(myLaptop);

	let oldLaptop = Laptop('Portal R2E CCMC', 1980);
	console.log('Result form creating instance without new keyword');
	console.log(oldLaptop); // result undefined because you called the Laptop function instead of creating a new object instance. Laptop function does not have a return statement as well.

	//Mini activity: 3 more instances

	let ownLaptop = new Laptop('Macbook Pro', 2021);
	console.log('Result form creating objects using object constructor');
	console.log(ownLaptop);

	let workLaptop = new Laptop('HP Elitebook 840', 2021);
	console.log('Result form creating objects using object constructor');
	console.log(workLaptop);

	let dreamLaptop = new Laptop('Alienware X16', 2023);
	console.log('Result form creating objects using object constructor');
	console.log(dreamLaptop);

		//could also be:
		/*
		let laptop2 = new Laptop('Macbook Pro', 2021)
		let laptop3 = new Laptop('Macbook Pro', 2022)
		let laptop4 = new Laptop('Macbook Pro', 2023)
		console.log(laptop2);
		console.log(laptop3);
		console.log(laptop4);*/


	let computer = {}
	let myComputer = new Object()
	console.log(computer); // result {} empty object
	console.log(myComputer);

//Access object properties
	console.log('Result of dot notation: ' + myLaptop.name); //dot notation method
	console.log('Result of dot notation: ' + myLaptop.manufactureDate);
	console.log('Result: ' + myLaptop['name']); //square bracket method, usually used with arrays

//Access array objects
	//can be done using []

	let array = [laptop1, myLaptop];
	console.log(array[0]['name']); // may be confused for accessing array indexes
	console.log(array[0].name); // this tells us that array is an object by using the dot notation

//Initialize, Add, Delete, & Reassign Object Properties
	let car = {}; 
	car.name = 'Honda Civic'
	console.log("result of adding properties in an empty object:")
	console.log(car)

	car.number = [1,2,3];
	console.log(car)

	car['manufacture date'] = 2019;
	console.log(car['manufacture date']);
	//console.log(car.manufacture date); // this will result an error since dot notation does not allow to use spaces in properties
	console.log(car)

	console.log(car.manufactureDate); // result undefined, we can have undefined values in an object


	//Deleting object properties
		delete car['manufacture date'];
		console.log('Result from deleting properties');
		console.log(car);


	//Reassign object properties
		car.name = 'Dodge Charger R/T';
		console.log('Result from reassigning properties: ')
		console.log(car);


	//Object Methods
		// method is a function which is a property of an object
		// similar to functions of real world objects, methods are defined based on what an object is capabale of doing and how it should work

			let person = {
				name: "Cardo",
				talk: function(){ //talk is a method
					console.log('Hello my name is ' + this.name) // this access property within an object
				}
			}
			console.log(person);
			console.log('Result of object methods: ')
			person.talk()

	//adding another function in an object
			person.walk = function(){
				console.log(this.name + ' walked 25 steps forward!')
			}
			person.walk()

	//object within an object
			let friend = {
				firstName: 'Nami',
				lastName: 'Misko',
				address: {
					city: 'Tokyo',
					country: 'Japan'
				},
				emails: ['nami@sea.com','namimisko@gmail.com'],
				introduce: function(){
					console.log('Hello! my name is ' + this.firstName + ' ' + this.lastName);
				}

			}
			friend.introduce();


//[Real World Application of Object]
	/*
		Pokemns interacting with each other with same stats, properties, functions
	*/

	//Use object literals first
		let myPokemon = {
			name: "Pikachu",
			level: 3,
			health: 100,
			attack: 50,
			tackle: function(){
				console.log("This pokement tackled Target Pokemon!")
				console.log("Target Pokemon's health is now reduced to Target Pokemon Health")
			},
			faint: function(){
				console.log("Pokemon fainted")
			}
		}
		console.log(myPokemon);
		myPokemon.tackle();
		myPokemon.faint();


	//Use object constructors next 
		function Pokemon(name,level){

			//properties
			this.name = name;
			this.level = level;
			this.health = 2 * level;
			this.attack = level;

			//methods
			this.tackle = function(target){
				console.log(this.name + ' tackled ' + target.name);
				console.log(target.name + "'s health is now reduced to _targetPokemonHealth_");
			}
			this.faint = function(){
				console.log(this.name + ' fainted');
			}
		}
	//instances
		let pikachu = new Pokemon("Pikachu",16);
		let rattata = new Pokemon('Rattata',8);
		pikachu.tackle(rattata);
		rattata.faint();
