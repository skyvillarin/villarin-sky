// Servers can actually respond differently with different requests
		// We start a request with a URL

const http = require('http'); // creates a variable port to store por number
const port = 4000;

const app = http.createServer((request,response)=>{

	// info about the URL endpoint of the req is in the request object
	// route - process or way to respond differently to a request

	if(request.url == '/greeting'){
		response.writeHead(200,{'Content-type':'text/plain'})
		response.end('Hello, B297!')
	}
	else if (request.url == '/homepage'){
		response.writeHead(200,{'Content-type':'text/plain'})
		response.end('This is the homepage')
	}


	//Mini activity - create an else condition that all other routes will return a message of page not available
	else {
  		response.writeHead(404, {'Content-type':'text/html'})
  		response.end(`
      <!DOCTYPE html>
      <html>
        <head>
          <title>404 Not Found</title>
        </head>
        <body>
          <h1>404 Not Found</h1>
          <p>Page not available</p>
        </body>
      </html>
    `)
	}

})
app.listen(port); // port instead of 4000 for reusability
console.log(`Server is now accessible at localhost: ${port}`)

