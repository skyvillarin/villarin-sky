let http = require("http");
let port = 4000;

let directory = [
{
	"name":"Zara Evergreen",
	"email": "zara.evergreen@gmail.com"
},
{
	"name":"Joy Boy",
	"email": "joyboy@gear5.com"
}

]

let app = http.createServer((request,response)=>{
	if (request.url == "/users" && request.method == "GET"){
		response.writeHead(200,{'Content-type':'application/json'})
		response.write(JSON.stringify(directory))
			// send data to the client
		response.end();
			// response.end finalizes the response
	}
	else if (request.url == "/users" && request.method == "POST"){
		// Route to add a new user, we have to receive an input from the client by adding a way to receive that input in 2 steps:

			//1st step: data step
					// will read the incoming stream of data from the client/postman and process it so we can save it in the requestBody variable
		
		let requestBody = '';
			// requestBody - placeholder to contain the data passed from the client
		request.on('data',(data)=>{
			console.log (`This is the data received from the client ${data}`)
				//console.log(data) - data stream, this will show in the console the data posted by client in postman
			requestBody += data
		})

			//2nd step: end step
					// will run once or after the data has been completely sent from the client

		request.on('end',()=>{
			console.log (`This is the requestBody: ${requestBody}`)
			console.log (typeof requestBody)
			requestBody = JSON.parse(requestBody)
				// initially, requestBody is in JSON format. We cannot add this to our directory array because it's a string
					// thus, we have to update requestBody variable with a parsed version of the received JSPN format data

			let newUser = {
			"name": requestBody.name,
			"email": requestBody.email
		}

			console.log(requestBody);
			console.log(typeof requestBody);
			console.log(`This is the email: ${requestBody.email}`)

			directory.push(newUser);
			console.log(`This is the email: ${newUser.email}`)

			response.writeHead(200,{'Content-type':'application/json'})
			//response.write(JSON.stringify(directory))
			response.write(JSON.stringify(newUser.name))
			response.end()
		})

		

	}


})
app.listen(port,()=>console.log(`Server running at localhost:${port}`))