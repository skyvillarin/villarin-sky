// MongoDB Aggregation
/*
	Join data from multiple documents
	Gives access to manipulate, fileter, and compute results providing us with information to make necessary development decisions without having to create a Frontend application
*/

//db market_db
//collection fruits
// Create documents to use for discussion
	db.fruits.insertMany([
		{
			name: "Apple",
			color: "Red",
			stock: 20,
			price: 40,
			supplier_id:1,
			onSale: true,
			origin: ["Philippines","US"]
		},
		{
			name: "Banana",
			color: "Yellow",
			stock: 15,
			price: 20,
			supplier_id:2,
			onSale: true,
			origin: ["Philippines","Ecuador"]
		},
		{
			name: "Kiwi",
			color: "Green",
			stock: 25,
			price: 50,
			supplier_id:1,
			onSale: true,
			origin: ["US","China"]
		},
		{
			name: "Mango",
			color: "Yellow",
			stock: 10,
			price: 120,
			supplier_id:2,
			onSale: false,
			origin: ["Philippines","India"]
		},
		])

	//Use the aggregate method
	/*
		Syntax: 
			db.collectionName.aggregate([
				{ $match: {fieldA: valueA} },
				{ $group: {_id: "$fieldB"}, result:{operation}}
			])

			$match - used to pass the documents that meet the specified conditions to the next pipeline stage/aggregation process
				Syntax:
					{ $match: {fieldA: valueA} }

			$group - used to group elements together and field-value pairs using the data from the grouped elements
				Syntax:
					{$group: {_id: "value", fieldresult: "valueResult" }}
	*/

			db.fruits.aggregate([
					{$match: {onSale: true}},
					{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
				])

			//_id 1, total 45
			//_id 2, total 15 (results in ascending order)


		//Field projection with aggregation
		/*
			The $project can be used when we are aggregating data to include/exclude fields from the returned results
			Syntax
				{$project: {field:1/0}}
		*/

			db.fruits.aggregate([
					{$match:{onSale: true}},
					{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
					{$project: {_id:0}}
				])
				//result
					//total 15
					//total 45

		//Sorting aggregated results
			/* $sort - used to change the order of aggregated results
					Syntax
						{$sort: {field: 1/-1} }

			*/
			db.fruits.aggregate([
					{$match:{onSale: true}},
					{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
					{$sort: {total: -1}}
				])
				// result, the totals will be in descending order: 45 15
				// using 1 will be ascending order 15 45

		//Aggregate values based on array fields
			/*
				$unwind - deconstructs an array field from a collection/field with an array value to output a result for each element
					Syntax:
						{$unwind: field}
			*/

			db.fruits.aggregate([
					{$unwind: "$origin"}
				])
				// results, each value in array will produce 1 result each in mongoDB (8 separate results)

			db.fruits.aggregate([
					{$unwind: "$origin"},
					{$group: {_id:"$origin", kinds: {$sum: 1}}} 
						//$sum calculates the collective sum of number values
						// 1 is used to increment the count for each group by 1 
				])
				// results will show sum of fruits per origin/country


			//Mini-activity: find how many fruits are yellow, use $match and $count
			
			//Case-sensitive version
				db.fruits.aggregate([
				  {$match: {color: "Yellow"}},
				  {$count: "yellowFruits"}
				])

			//Case-insensitive version with $regex
				db.fruits.aggregate([
				  {$match: {color: {$regex:'yEllow', $options:'i'}}},
				  {$count: "yellowFruits"}
				])


			//find how many fruit stocks that are less than or equal to 10 using $match and $count
				db.fruits.aggregate([
				  {$match: { stock: {$lte: 10 }}},
				  {$count: "lowOnStocks"}
				])

			//find fruits that are on sale for each group of supplier that has the maximum stocks using $match and $group, use $max for the stocks
				db.fruits.aggregate([
				  {$match: { onSale: true }},
				  {$group: {_id: "$supplier_id", max_Stock: {$max: "$stock" }}}
				])
				//$max - returns the highest value among all entries








