console.log("Hello, B297!");

//if, else if, and else statements

	//if statement - executes a statement if a specified condition is true
		let numG = -1;
		if(numG < 0){
			console.log("Hello! The condition in the if statement is true!")
		} //--will show since statement is true

	//else if statement
	//optional; can be added to capture additional conditions to change the flow of a program	
		let numH = 1;
		if(numG > 0){
			console.log("Hello")
		}
		else if(numH>0){
			console.log("This will log if the else if condition is true and the if condition is true!")
		}//--will show log of numH since previous condition is false

	//else statement
	//executes a statement if all other conditions are false
	// the "else" statement is OPTIONAL and can be added to capture any other result to change the flow of our program
		if (numG > 0){
			console.log("I'm enchanted to meet you!")
		}
		else if(numH = 0){
			console.log("It's me, Hi...")
		}
		else {
			console.log("Hello from the other side! This will log if the if and else if contions are not met!")
		}

//if, else if, and else with functions

	let message = "No message!";
	console.log(message);
		function determineTyphoonIntensity(windSpeed){
			if (windSpeed < 30){
				return 'Not a typhoon yet!'
			}
			else if (windSpeed <= 61){
				return 'Tropical Depression detected!'
			}
			else if (windSpeed >= 62 && windSpeed <=88){
				return 'Tropical Storm detected!'
			}
			else if (windSpeed >= 89 && windSpeed <=117){
				return 'Severe Tropical Storm detected!'
			}
			else {
				return 'Typhooon detected!'
			}
		}
		message = determineTyphoonIntensity(65)
		console.log(message);

	if (message == 'Tropical Storm detected!'){
		console.warn(message);//--console.warn() is a good way to print warnings in our console that can help us devs act on certain output within our code
	}

//[truthy and falsy]
/* 	- in JS, "truthy" value is a value that is considered true when encountered in a Boolean context
	- Values are considered true unless defined false

	- Falsy values/exceptions for truthy:
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN
*/

	//Truthy examples
		if (true){
			console.log('This is truthy!')
		}
		if (1){
			console.log('This is truthy!')
		}
		if([]){
			console.log('This is truthy!')
		}

	//Falsy examples
		if (false){
			console.log('This will not log in the console')
		}
		if (0){
			console.log('This will not log in the console')
		}
		if (undefined){
			console.log('This will not log in the console')
		}//will not show since values are not true

		if("Sky"){
			console.log("This will log in the console.")
		}//will show since value is not falsy value

//[conditional (ternary) operator]
/* Takes in three operands:
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is falsy

	Commonly used for single statement execution where the result is of only 1 line of code

	Syntax:
		(condition) ? ifTrue : ifFalse
*/

	//single statement execution
	let ternaryResult = (1 < 18) ? true /*can be"This is true!"*/ : false;
	console.log("Result of ternary operator: " + ternaryResult);

	//multiple statement execution
	let name;
	function isOfLegalAge(){
		name = 'John';
		return 'You are of the legal age limit!';
	}
	function isUnderAGE(){
		name = 'Jane';
		return 'You are under the legal age limit!';
	}

	/*let age = parseInt(prompt("What is your age?")) //prompt will show as string, using parseInt will show integer or number inputted
	console.log(age);
	let legalAge = (age > 18) ? isOfLegalAge() : isUnderAGE();
	console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);*/

//[switch statement]
	//can be used as an alternative to an if, else if, and else statement where the data to be used in the condition is of an EXPECTED input
	/* Syntax
		switch (expression){
		case value:
			statement;
			break;
		default:
			statement;
			break;
		} */

	/*let day = prompt("What day of the week is it today?").toLowerCase();
	console.log(day);


		switch (day) {
			case 'monday':
				console.log("The color of the day is blue!");
				break;
			case 'tuesday':
				console.log("The color of the day is yellow!");
				break;
			case 'wednesday':
				console.log("The color of the day is red!");
				break;
			default:
				console.log("Please input a valid day")
				break;
		//Create cases for thursday, friday, saturday, sunday
			case 'thursday':
				console.log("The color of the day is pink!");
				break;
			case 'friday':
				console.log("The color of the day is green!");
				break;
			case 'saturday':
				console.log("The color of the day is orange!");
				break;
			case 'sunday':
				console.log("The color of the day is black!");
				break;

		}*/
		
//[try-catch-finally statement]
	//try catch statements are commonly used for error handling

	function showIntensityAlert(windSpeed){
		try {//this is where we attempt to execute a code
			alerat(determineTyphoonIntensity(windSpeed));
		}
		catch (error){//where we catch errors
			console.log(typeof error);
			console.warn(error.message);//used to access information relating to the error object
		}
		finally {
			alert('Intensity updates will show alert')
		}
	}
	showIntensityAlert(56)
	console.log("Hi!")














