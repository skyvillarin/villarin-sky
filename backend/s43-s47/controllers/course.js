const Course = require("../models/Course");
const User = require("../models/User");

// ACTIVITY: Add Course Controller
module.exports.addCourse = (req, res) => {
  const newCourse = new Course({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  });

  return newCourse.save().then((course, error) => {
      if (error) {
        return res.send(false);
      } else {
        console.error(newCourse);
        return res.send(true);
      }
    })
    .catch((err) => res.send(err));
};

// Retrieve all courses
/*
  Retrieve all the courses from the database
*/

//We will use the find() method for our course model
module.exports.getAllCourses = (req,res)=>{
  return Course.find({}).then(result=>{
    return res.send(result)
  })
}

//getAllActiveCourses - activity
module.exports.getAllActiveCourses = (req,res)=>{
  return Course.find({isActive:true}).then(result=>{
    return res.send(result)
  })
}

//Get a specific course
module.exports.getCourse = (req,res)=>{
  return Course.findById(req.params.courseId).then(result=>{
    return res.send(result)
  })
}

//edit a specific course
module.exports.updateCourse = (req,res)=>{

  let updatedCourse = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price
  }

  return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course, error)=>{
    if(error){
      return res.send(false);
    }else{
      return res.send(true);
    }
  })
}


//group activity: archive controller
module.exports.archiveCourse = (req,res) => {

  let updatedIsActive = {
    isActive : false
  }

  return Course.findByIdAndUpdate(req.params.courseId, updatedIsActive).then((course,error)=> {
    if (error) {
      return res.send(false);
    }else {
      res.send(true);
    }
  })

}

// group activity: create a controller method "activateCourse" for activating a course obtaining the course ID from the request params. Simply update the course isActive status into true


// module.exports.activateCourse = (req, res) => {
//   const courseId = req.params.courseId;

//   return Course.findByIdAndUpdate(courseId, { isActive: true }).then(
//     (course, error) => {
//       if (error) {
//         return res.send(false);
//       } else {
//         return res.send(true);
//       }
//     }
//   );
// };

module.exports.activateCourse = (req, res) => {
  return Course.findByIdAndUpdate(req.params.courseId)
    .then(savedCourse => {
      if (!savedCourse) {
        return res.send(false); 
      }
      if (savedCourse.isActive) {
        return res.send("Course already active"); 
      }
      savedCourse.isActive = true;
      savedCourse.save()
        .then(() => {
          return res.send(true); 
        })
        .catch(saveError => {
          console.error(saveError);
          return res.send(false); 
        });
    })
    .catch(error => {
      console.error(error);
      return res.send(false); 
    });
};

// Controller action to search for courses by course name
module.exports.searchCoursesByName = async (req, res) => {
  try {
    const { courseName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const courses = await Course.find({
      name: { $regex: courseName, $options: 'i' }
    });

    res.json(courses);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


// Controller action to get emails of enrolled users
module.exports.getEmailsOfEnrolledUsers = async (req, res) => {
  const courseId = req.params.courseId; // Use req.params instead of req.body for route parameters

  try {
    // Find the course by courseId
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: 'Course not found' });
    }

    // Get the userIds of enrolled users from the course
    const userIds = course.enrollees.map(enrollee => enrollee.userId);

    // Find the users with matching userIds
    const enrolledUsers = await User.find({ _id: { $in: userIds } }); // Use userIds instead of users

    // Extract the emails from the enrolled users
    const emails = enrolledUsers.map(user => user.email); // Use map instead of forEach

    res.status(200).json({ userEmails: emails }); // Correct the response object property name
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while retrieving enrolled users' });
  }
};


//ACTIVITY 

// Controller for searching courses by price range
module.exports.searchByPrice = async (req, res) => {
  try {
    const { minPrice, maxPrice } = req.body;

    if (!minPrice || !maxPrice) {
      return res.status(400).json({ error: 'Both minPrice and maxPrice are required' });
    }

    const courses = await Course.find({
      price: { $gte: minPrice, $lte: maxPrice },
    });

    res.json(courses);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while searching for courses' });
  }
};









  
    


 








