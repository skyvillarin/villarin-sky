//Route

//Dependencies and Modules

const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const {verify, verifyAdmin} = auth; //destructure process

//Routing Component
const router = express.Router();

//Routes - POST
//Check email
router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController))
})

//Register a user
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

//User Authentication
router.post("/login",userController.loginUser);

//ACTIVITY
router.post("/details", verify, userController.getProfile);

//Route to enroll user to a course
router.post("/enroll", verify, userController.enroll);

// ACTIVITY: Retrieve enrolled courses of the logged-in user
router.get("/getEnrollments", verify, userController.getEnrollments);

// POST route for resetting the password
router.post('/reset-password', verify, userController.resetPassword);

// Update user profile route
router.put('/profile', verify, userController.updateProfile);

//ACTIVITY
// Update User as Admin
router.put("/update-admin/:userId", verify, verifyAdmin, userController.updateUserAsAdmin
);

// Stretch goal
router.put('/update-enrollment-status', verify, verifyAdmin, userController.updateEnrollmentStatus);

//Export Route System
module.exports = router;

