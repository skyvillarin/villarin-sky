const courseSchema = new mongoose.Schema({
	firstName: {
		type : String,
		required : [true,"FirstName is required"]
	},
	lastName: {
		type: String,
		required : [true,"LastName is required"]
	},
	email: {
		type: String,
		required : [true,"Email is required"]
	},
	password: {
		type: String,
		required : [true,"Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: true
	},
	mobileNo: {
		type: String,
		required : [true,"MobileNo is required"]
	},
	enrollments: [
			{
				courseId: {
					type: String,
					required: [true,"CourseId is required"]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: Enrolled()
				},
			}
		]
});
//Model
module.exports = mongoose.model("User",userSchema);