//CRUD Operations
	/*
		- The heart of any backend application
		- Mastering the CRUD operations is essential to any developer
			- makes us a valuable developer and makes the work easier for us to deal with huge amounts of information
	*/

	// to switch DBs, use the command "use <db name>"


//CREATE
	//Insert one document
		//Syntax: 
			//db.collectionName.insertOne({object});

		db.users.insertOne({
			"firstName":"John",
			"lastName":"Smith"
		})

		db.users.insertOne({
			firstName:"Jane",
			lastName:"Doe",
			age: 21,
			contact: {
				phone: "87000",
				email:"janedoe@gmail.com"
			},
			courses: ["CSS","JavaScript","Python"],
			department: "none"
		})

	//Insert Many
		//Syntax:
			//db.collectionName.insertOne([{objectA},{objectB}]);

		db.users.insertMany([
			{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: "87001",
				email:"stephenhawking@gmail.com"
			},
			courses: ["Python","React","PHP"],
			department: "none"
		},
		{
			"firstName": "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "87002",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React","Laravel","Sass"],
			department: "none"
		}
		])


//READ/RETRIEVE
	//Find
		//Finding a single document
			//leaving the search criteria empty will retrieve ALL the documents
			/*
				db.collectionName.find()
				db.collectionName.find({field:value})
					*/
			db.users.find();

			db.users.find({firstName:"Stephen"})

		//Finding document with multiple parameters
			/*
				db.collectionName.find({fieldA:valueA},{fieldB:valueB})
					*/
			db.users.find({lastName: "Armstrong", age:82})

//UPDATE
	//Updating a single document
		//let's add another document:
			db.users.insertOne({
				firstName: "Test",
				lastName: "Test",
				age: 0,
				contact: {
					phone: "00000",
					email: "test@gmail.com"
				},
				courses:[],
				department: "none"
			})

		/*
			Syntax:
				db.collectionName.updateOne({critera},{$set:{field,value}});
		*/

			db.users.updateOne(
				{firstName: "Test"},
				{
					$set:{ //$set operator replaces the value of a field with the specified value
						firstName: "Bill",
						lastName: "Gates",
						age: 65,
						contact: {
							phone: "12345678",
							email: "bill@gmail.com"
						},
						courses: ["PHP","Laravel","HTML"],
						department: "Operations",
						status: "active"
					}
				}
			)

			db.users.find({firstName: "Bill"}) // status is inserted automatically

	//Update multiple documents
		/*
			Syntax:
				db.collectionName.updateMany({criteria},{$set:{field,value}})
		*/

			db.users.updateMany(
				{department: "none"},
				{
					$set: {department: "HR"}
				}
			)

//DELETE
	//create a document to delete
			db.users.insert({
				firstName: "test"
			})

		/*
			Syntax:
				db.collectionName.deleteOne({criteria})
		*/
			db.users.deleteOne({
				firstName: "test"
			})

	//Delete Many
		/*
			Be careful when using the deleteMany method. If we do not add a search criteria, it will DELETE ALL DOCUMENTS in a db
			DO NOT USE: databasename.collectionName.deleteMany()
			Syntax:
				db.collectionName.deleteMany({criteria})
		*/

			db.users.deleteMany({
				firstName: "Bill"
			})




			db.rooms.updateOne(
				{name: "queen"},
				{
					$set:{ 
						rooms_available: 1
					}
				}
			)











