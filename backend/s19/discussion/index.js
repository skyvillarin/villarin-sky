//Syntax, Statements, and Comments

//For us to create a comment, we use Ctrl+/

console.log("We tell the computer to log this in the console.");
alert("We tell the computer to display an alert with this message!");

// Console is for output and debugging

//Statements - in programming are instructions that we tell the computer to perform
//JS Statements usually end with semicolon (;)

//Syntax - it is the set of rules that describes how statements must be constructed

//Comments
//For us to create a comment, we use Ctrl + /
//We have single line and multi-line comments

// console.log("Hello!");

// alert("This is an alert!");
// alert("This is another alert");

//Whitespaces (spaces and line breaks) can impact functionality in many computer languages, but not in JS.


//Variables
//This is used to contain data
/*
	    Guides in writing variables:
	        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
	        2. Variable names should start with a lowercase character, use camelCase for multiple words.
	        3. For constant variables, use the 'const' keyword.
	        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.

	    Best practices in naming variables:

			1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains.

				let firstName = "Michael"; - good variable name
				let pokemon = 25000; - bad variable name

			2. When naming variables, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with capital letters. Because there are several keywords in JS that start in capital letter.

				let FirstName = "Michael"; - bad variable name
				let firstName = "Michael"; - good variable name

			3. Do not add spaces to your variable names. Use camelCase for multiple words, or underscores.

				let first name = "Mike";

			camelCase is when we have first word in small caps and the next word added without space but is capitalized:

				lastName emailAddress mobileNumber

			Underscores sample:

			let product_description = "lorem ipsum"
			let product_id = "250000ea1000"

	*/


//Declare variables
	//tell our devices that a variableName is ready to store data
	//Syntax
		//let/const variableName;

	let myVariable;
	console.log(myVariable);//undefined

	//Variables must be declared first before they are used
	//using variables before they are declared will return an error
	let hello;
	console.log(hello);

	//declaring and initializing variables
	//Syntax
		//let/const variableName = initialvalue;

	let productName = 'desktop computer'; //'desktop computer' aka "string", = is aka "literals"
	console.log(productName);

	let productPrice = 18999;
	console.log(productPrice);

	const interest = 3.539; //let can be changed in the future, const are variables that should not change

	//let and const are used to create a variable

	//re-assigning variable values

	productName = 'Laptop';
	console.log(productName);

	//interest = 3.5;
	console.log(interest);

	//let variable cannot be re-declared within its scope
	let friend = 'Kate';
	friend = 'Jane'; //Uncaught SyntaxError: Identifier 'friend' has already been declared - remove let for duplicate variable


	//Declare a variable
	let supplier;

	//initialization is done after the variable has been declared
	supplier = "John Smith Tradings";

	//re-assignment of value (initial value already declared)
	supplier = "Zuitt Store";


	//We cannot declare a const variable without inititalization
	/*const pi;
	pi = 3.1416;
	console.log(pi);*/

	//Multiple variable declarations

	let productCode = 'DCO17', productBrand = 'Dell';
	/*let	productCode = 'DC017';
	const productBrand = 'Dell';*/
	console.log(productCode, productBrand);


	//Data Types

	//Strings
	// ' or " quotes are known as String Literals
	//a series of characters that create a word, phrase, sentence etc. related to creating text
	let country = 'Philippines';
	let province = "Metro Manila";

	//Concatenating Strings
	let fullAddress = province + ', ' + country
	console.log(fullAddress);

	let greeting = 'I live in the ' + country;
	console.log(greeting);

	//the escape character (\)
	// \ in combination with other characters can produce different effects
	//"\n" refers to creating a new line in between text

	let mailAddress = 'Metro Manila \n\nPhilippines';
	console.log(mailAddress);

	let message = "John's employees went home early";
	console.log(message);
	message = 'John\'s employees went home already'; //use backslash when you want to use an apostrophe if using single quotes

	//Numbers - digits that can be calculated (cellphone numbers cannot be calculated so should be in strings and enclosed with quotations)
	let headcount = 26;
	console.log(headcount);
	let grade = 98.7;
	console.log(grade);
	let planetDistance = 2e10;
	console.log(planetDistance);

	//Combine text and strings
	console.log("John's first grade last quarter is " + grade);

	//Boolean: true or false - another data type aside from string and numbers
	let isMarried = false;
	let isGoodConduct = true;
	console.log(isMarried);

	console.log("isGoodConduct: " + isGoodConduct);

	//Arrays 
	// Arrays are a special kind of data type that's used to store multiple values
	// Arrays can store different data types but is normally used to store similar data types
	//Best practice is to store elements with similar data types

	//Syntax
		//let/const arrayName = [elementA, elementB, ...]

		// [ ] symbols aka Array Literals
	let grades = [98.7, 92.1, 90.7, 98.6];
	console.log(grades);

	//Objects
	// {} aka object literals
	// Objects are another special kind of data type that's used to mimic real world objects/items
	// They're used to create complex data that contains pieces of information that are relevant to each other
	// Every individual piece of information is called a property of the object

	// Syntax
	    // let/const objectName = {
	    //     propertyA: value,
	    //     propertyB: value,
	    // }

	let myGrades = {
		firstGrading: 98.7,
		secondGrading: 92.1,
		thirdGrading: 90.7,
		fourthGrading: 94.6
	}
	console.log(myGrades);

	let person = {
		fullName: 'Juan Dela Cruz',
		age: 35,
		isMarried: false,
		contact: ["+639123456789", "87000"],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	}
	console.log(person);


//typeof operator is used to determine the type of data or the value of a variable. It outputs a string.
console.log(typeof person);//object

//Note: Array is a special type of object with methods and functions to manipulate it. We will discuss these methods in later sessions. (Javascript - Array Manipulation)
console.log(typeof grades);

	//Null
	//it is used to intentionall express the absence of the value in a variable declaration/initialization

	let spouse = null;

	//Undefined
	//this represents the state of a variable that has been declared but without an assigned vlaue

	let	fullName;
	console.log(fullName);
