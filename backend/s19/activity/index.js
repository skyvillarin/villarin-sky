

console.log("Hello World");

function countLetter(letter, sentence) {
    if (typeof letter !== 'string' || letter.length !== 1) {
        return undefined; // Invalid input
    } else {
        let count = 0;
        for (let i = 0; i < sentence.length; i++) {
            if (sentence[i] === letter) {
                count++;
            }
        }
        return count === 0 ? undefined : count;
    }
}

const result = countLetter('a', 'boy');
console.log(result); // Output: undefined



function isIsogram(text) {
  // Convert the text to lowercase to disregard casing
  const lowercaseText = text.toLowerCase();

  // Create an empty object to store seen characters
  const seenChars = {};

  // Loop through each character in the lowercase text
  for (let i = 0; i < lowercaseText.length; i++) {
    const char = lowercaseText[i];

    // If the character has already been seen, it's not an isogram, so return false
    if (seenChars[char]) {
      return false;
    }

    // Mark the character as seen
    seenChars[char] = true;
  }

  // If the loop completes without finding repeating letters, it's an isogram, so return true
  return true;
}

// Example usage:
console.log(isIsogram('Dermatoglyphics')); // Output: true
console.log(isIsogram('aba')); // Output: false
console.log(isIsogram('hello')); // Output: false


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    const ignoreTextCase = text.toLowerCase()

    for (let i = 0; i < ignoreTextCase.length; i++) {
        for (let j = i + 1; j < ignoreTextCase.length; j++) {
            if (ignoreTextCase[i] === ignoreTextCase[j]) {
                return false; 
            }
        }
    }

    // If no repeating characters were found, it's an isogram
    return true;
}

// Example usage:
console.log(isIsogram('boy'));     // Output: false (repeating 'l')
console.log(isIsogram('world'));     // Output: true (no repeating characters)
console.log(isIsogram('programming'));// Output: false (repeating 'g' and 'r')




function purchase(age, price) {
    // Check if the age is below 13
    if (age < 13) {
        return undefined;
    } else if (age >= 13 && age <= 21) {
        // Apply a 20% discount for students aged 13 to 21
        const discountedPrice = (price * 0.8).toFixed(2);
        return discountedPrice.toString();
    } else if (age >= 65) {
        // Apply a 20% discount for senior citizens aged 65 and above
        const discountedPrice = (price * 0.8).toFixed(2);
        return discountedPrice.toString();
    } else {
        // For people aged 22 to 64, return the rounded off price as a string (2 decimal places)
        const roundedPrice = price.toFixed(2);
        return roundedPrice.toString();
    }
}

// Example usage:
console.log(purchase(13, 100));   // Output: undefined (below 13)
console.log(purchase(18, 333));   // Output: "80" (20% discount for students)
console.log(purchase(70, 100));   // Output: "80" (20% discount for senior citizens)
console.log(purchase(30, 100));   // Output: "100" (rounded off for others)


function isIsogram(text) {
//     // An isogram is a word where there are no repeating letters.
//     // The function should disregard text casing before doing anything else.
//     // If the function finds a repeating letter, return false. Otherwise, return true.

  const lowercaseText = text.toLowerCase();
  const seenLetters = {};

  // Loop through each character in the lowercase text
  for (let letters of lowercaseText) {
    // If the character is already in the seenChars object, it's not an isogram
    if (seenLetters[letters]) {
      return false;
    }
    
    // Otherwise, mark the character as seen by adding it to the object
    seenLetters[letters] = true;
  }

  // If no repeating characters were found, it's an isogram
  return true;
 }


// Example usage:
const isogram = 'Machine';
const notIsogram = 'Hello';

console.log(isIsogram(isogram));     // Output: true
console.log(isIsogram(notIsogram));  // Output: false



function findHotCategories(items) {
  // Create an object to store categories as keys
  const categoryMap = {};

  // Iterate through the items
  for (const item of items) {
    // If the stocks for an item are 0 and the category is not already in the map, add it
    if (item.stocks === 0 && !categoryMap[item.category]) {
      categoryMap[item.category] = true;
    }
  }

  // Extract the keys (unique categories) from the map as an array
  const hotCategories = Object.keys(categoryMap);

  return hotCategories;
}

// Example usage:
const items = [
  { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
  { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
  { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
  { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
  { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
];

const results = findHotCategories(items);
console.log(results); // Output: ['toiletries', 'gadgets']


function findFlyingVoters(candidateA, candidateB) {
  // Create an array to store flying voters
  const flyingVoters = [];

  // Iterate through the voters of candidate A
  for (const voterA of candidateA) {
    // Check if the voter also voted for candidate B
    if (candidateB.includes(voterA)) {
      // If so, add the voter to the flyingVoters array
      flyingVoters.push(voterA);
    }
  }

  return flyingVoters;
}

// Example usage:
const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

const resultss = findFlyingVoters(candidateA, candidateB);
console.log(resultss); // Output: ['LIWf1l', 'V2hjZH']

// /*
//     1. Create the following variables to store to the following user details:

//     Variable Name - Value Data Type:
    
//     firstName - String
//     lastName - String
//     age - Number
//     hobbies - Array
//     workAddress - Object

//     The hobbies array should contain at least 3 hobbies as Strings.
    
//     The work address object should contain the following key-value pairs:

//             houseNumber: <value>
//             street: <value>
//             city: <value>
//             state: <value>

//     Log the values of each variable to follow/mimic the output.

//     Note: strictly follow the variable names.
// */

//     //Add your variables and console log for objective 1 here:
// let firstName = "John"
// console.log("First Name: " + firstName);

// let lastName = "Smith"
// console.log("Last Name: " + lastName);

// let age = 30
// console.log("Age: " + 30);

// let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
//     console.log(hobbies);

// let workAddress = {
//         address: {
//             houseNumber: '32',
//             street: 'Washington',
//             city: 'Lincoln',
//             state: 'Nebraska'
//         }
//     }
// console.log(workAddress);

// /*          
//     2. Debugging Practice - Identify and implement the best practices of creating and using variables 
//        by avoiding errors and debugging the following codes:

//             -Log the values of each variable to follow/mimic the output.

//         Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
// */  

//     let fullName = "Steve Rogers";
//     console.log("My full name is: " + fullName);

//     let currentAge = true;
//     console.log("My current age is: " + currentAge);
    
//     let friends = ["Tony","Bruce","Thor","Natasha", "Clint", "Nick"];
//     console.log("My Friends are: " + friends)

//     let profile = {
//         username: "captain_america",
//         fullName: 'Steve Rogers',
//         age: 40,
//         isActive: false,
//     }
//     console.log("My Full Profile: " + profile);
//     console.log(profile);

//     let fullName2 = "Bucky Barnes";
//     console.log("My bestfriend is: " + fullName);

//     let lastLocation = "Arctic Ocean";
//     lastLocation = "Atlantic Ocean";
//     console.log("I was found frozen in: " + lastLocation);



//     //Do not modify
//     //For exporting to test.js
//     //Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
//     try{
//         module.exports = {
//             firstName: typeof firstName !== 'undefined' ? firstName : null,
//             lastName: typeof lastName !== 'undefined' ? lastName : null,
//             age: typeof age !== 'undefined' ? age : null,
//             hobbies: typeof hobbies !== 'undefined' ? hobbies : null,
//             workAddress: typeof workAddress !== 'undefined' ? workAddress : null,
//             fullName: typeof fullName !== 'undefined' ? fullName : null,
//             currentAge: typeof currentAge !== 'undefined' ? currentAge : null,
//             friends: typeof friends !== 'undefined' ? friends : null,
//             profile: typeof profile !== 'undefined' ? profile : null,
//             fullName2: typeof fullName2 !== 'undefined' ? fullName2 : null,
//             lastLocation: typeof lastLocation !== 'undefined' ? lastLocation : null
//         }
//     } catch(err){
//     }