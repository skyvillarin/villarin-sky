console.log("Hello B297!");

//Functions
	//Parameters and Arguments

	/* function printName(){
		let nickname = prompt("Enter your nickname: ")
		console.log("Hi, " + nickname);
		}
		printName(); */
	
	function printName(name){
		console.log("Hi, " + name);
	}
	printName("Sky");//--example1: can use string

	let sampleVariable = "Cardo";
	printName(sampleVariable);//--example 2: can use variable

	function checkDivisibilityBy8(num){
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}
	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);
	checkDivisibilityBy8(9678);//--example 3

	/* 
	Mini-Activity
	- check the divisibility of a number by 4
	- have one parameter named num

	1. 56
	2. 95
	3. 444
	*/

	function checkDivisibilityBy4(num){
		let remainder = num % 4;
		console.log("The remainder of " + num + " divided by 4 is: " + remainder);
		let isDivisibleBy4 = remainder === 0;
		console.log("Is " + num + " divisible by 4?");
		console.log(isDivisibleBy4);
	}
	checkDivisibilityBy4(56);
	checkDivisibilityBy4(95);
	checkDivisibilityBy4(444);

//Functions as arguments
  //Function parameters can also accept other function as arguments

	function argumentFunction(){
	  console.log("This function was passed as an argument before the message was printed.");
	}
	function invokeFunction(argumentFunction){
	  argumentFunction();
	}
	invokeFunction(argumentFunction);
		//adding and removing the () impacts the output of JS heavily
	  	//when a function is used with (), it denotes invoking/calling a function
	  	//if used without (), it's normally associated with using the function as an argument to another function

//Using Multiple Parameters
	function createFullName(firstname, middlename, lastname){
		console.log(firstname + ' ' + middlename + ' ' + lastname);
	};
	createFullName('Juan','Dela','Cruz');
	createFullName('Cruz','Dela','Juan');//example1-strings - JS has no way of knowing which one is firstname or lastname
	createFullName('Juan','Dela'); // 3rd argument undefined
	createFullName('Juan','Dela','Cruz','III'); //III will not show since only up to 3

	let firstName = 'John';
	let middleName = 'Doe';
	let lastName = 'Smith';
	createFullName(firstName,middleName,lastName);//example2 - argurments are defined

	/*
	Mini activity
	-create a function called printFriends
	-3 paramaters friend1, friend2, friend3
	*/

	function printFriends(friend1,friend2,friend3){
		console.log("My three friends are: " + friend1 + ", " + friend2 +", " + friend3 +".")
	}
	printFriends("TinkyWinky", "Dipsy", "Lala");

//Return Statement
	function returnFullName(firstName, middleName, lastName){
		return firstName + ' ' + middleName + ' ' + lastName;
		console.log("This will not be printed"); // Will not be returned since the return code will not execute codes after it
	}	
	let completeName1 = returnFullName("Monkey","D","Luffy");
	let completeName2 = returnFullName("Cardo","Tanggol","Dalisay");
	console.log(completeName1 + " is my bestfriend!")
	console.log(completeName2 + " is my friend!")

	/* Mini Activity*/
	//Create a function that will calculate an area of a square //
		function getSquareArea(side) {
	  		return side **2;
		}
		let areaSquare = getSquareArea(4);
		console.log("The result of getting the area of a square with length of 4: ")
		console.log(areaSquare);

	//Create a function that will add 3 numbers
		function computeSum(num1, num2, num3) {
	  			return num1 + num2 + num3;
			}
				let sumOfThreeDigits = computeSum(1,2,3);
		console.log("The sum of three numbers are ");
		console.log(sumOfThreeDigits);

	//Create a function that will check if the number is equal to 100
		function compareToOneHundreed(num) {
	 		return num === 100;
			}
		let booleanValue = compareToOneHundreed(99);
		console.log("Is this one 100?"); 
		console.log(booleanValue);








	