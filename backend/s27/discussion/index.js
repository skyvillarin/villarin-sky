console.log("Array Manipulation!");
// Array Methods
	// JS has built-in functions and methods for arrays. This allows us to manipulate and access array items

	// Mutator methods
		/* Functions that mutate or change an array after they are created
			These methods manipulate the original array performing various tasks such as adding and removing elements
		*/
		let fruits = ['Apple','Orange','Kiwi','Dragon Fruit'];
		console.log(fruits);
		console.log(typeof fruits); // result object

		// Push
			// ADDS an element in the end of an array and returns the array's length
			/* Syntax:
					arrayName.push("itemToPush");
			*/
			console.log('Current array: ');
			console.log(fruits);
				//if we assign it to a variable we are able to save the length of the array
				let fruitsLength = fruits.push('Mango');
				console.log(fruitsLength);
				console.log('Mutated array from push method: ')
				console.log(fruits); // result mango is added
				//can add more than 1 variable

		// Pop
			// REMOVES the last element and returns the removed element
			/* Syntax
				arrayName.pop();
			*/
			let removedFruit = fruits.pop();
			console.log(removedFruit);
			console.log('Mutated array from pop method:');
			console.log(fruits);
			// cannot delete multiple elements

			/* Mini activity
				- create a function called removeFruit()
				- after invoking the function, log the fruits array in the console
			*/
			function removeFruit(){
				fruits.pop();
			}
			removeFruit();
			console.log(fruits); // result dragon fruit is deleted so 3 remaining fruits

		// Unshift
			// ADDS one or more elements at the BEGINNING of an array
			/* Syntax
				arrayName.unshift('elementA');
				arrayName.unshift('elementA','elementB');
			*/
			fruits.unshift('Lime','Banana');
			console.log('Mutated array from unshift method:');
			console.log(fruits); // result apple and banana are added in front of apple

			function unshiftFruit(fruit1,fruit2){
				fruits.unshift(fruit1,fruit2);
			}
			unshiftFruit('Grapes','Papaya');
			console.log(fruits); // result grapes and papaya will be added in the beginning

		// Shift
			// removes just one element at the beginning and returns the removed element
			/* Syntax
				arrayName.shift('elementA');
			*/
			let anotherFruit = fruits.shift();
			console.log(anotherFruit); // show the deleted item
			console.log('Mutated array from the shift method:')
			console.log(fruits); // result, grapes will be removed

		// Splice
			// removes then adds more than one element from a specified index number 
			/* Syntax
				arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
			*/
			fruits.splice(1, 2, 'Calamansi', 'Cherry');
			console.log('Mutated array from splice method:')
			console.log(fruits); // removes lime and adds calamansi and cherry in lieu of lime

			function spliceFruit(index,deleteCount,fruit){
				fruits.splice(index,deleteCount,fruit);
			}
			spliceFruit(1,1,"Avocado");
			spliceFruit(2,1, "Durian");
			console.log(fruits); //result calamansi replaced with avocado, cherry replaced with durian
			//if no value to be added, result will show undefined in place insteadc

		// Sort
			// arranges elements in alphanumeric order
			/* Syntax
				arrayName.sort();
			*/
			fruits.sort();
			console.log('Mutated array from sort method:')
			console.log(fruits);

		// Reverse
			//reverses the order of array alements (z-a)
			/* Syntax
				arrayName.reverse();
			*/
			fruits.reverse();
			console.log('Mutated array from reverse method:')
			console.log(fruits);

/*
Mini Activity

Methods that does not update the array 
	- Non mutator methods
Returns an array as a string but with a specified separator?
	- join()
Returns an array of a COPIED portion of an original array?
	-slice()
Returns an array as a string with comma as its separator?
	- toString()
Returns the index number of the argument provided that will match the LAST item that matches our argument?
	- lastIndexOf()
Returns the index number of the argument provided that will match the first element it finds?
	- indexOf()
These are methods that loop through all elements in an array
	- Iterator methods
Similar to a for loop that iterates on each array element.
	- forEach()
Iterates on each element AND returns new array with different values depending on the result of the function's operation
	- map()
Returns a boolean which determines if an item is in the array or not
	- includes()
Evaluates elements from left to right and returns/reduces the array into a single value
	- reduce()
Checks if the argument passed can be found in the array
	- filter()
Checks if at least one element in the array meets the given condition
	- some()
*/

			




