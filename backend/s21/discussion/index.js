console.log("Hi, B297!");

//

console.log("Madness? This is Spartaaaa!");
function printline(){
	console.log("Madness? This is Spartaaaa!");
};

printline();
printline();
printline();
printline();


//Functions
	//lines/blocks of code that tell our devices to perform a certain task when called/invoke
	/*Syntax: 
		function functionName() {
			code block (statement)
		}*/

	//Function Declaration
	function printName(){
			console.log("My name is Sky.")
	};
	//Function Invocation
	printName();
	
	declaredFunction();

	//Function declaration versus Expressions

		//Function Declaration
			//function declaration is created with the function keyword and adding a function name
			//they are saved for later use
			//can be hoisted

		function declaredFunction(){
			console.log("Hello from declaredFunction!")
		};

		declaredFunction();

		//Function Expression
			//function expression is stored in a variable
			//function expression is an anonymous function assigned to the variable function
			//cannot be hoisted
		let variableFunction = function() {
			console.log("Hello from function expression!")
		};

		variableFunction();

		//a function expression of function named funcName assigned to the variable funcExpression
		let funcExpression = function funcName() {
			console.log("Hello from the other side!");
		};

		funcExpression();

		//We can also reassign declared functions and function expressions to new anonymous functions
		declaredFunction = function(){
			console.log("updated declaredFunction")
		};

		declaredFunction();

		funcExpression = function(){
			console.log("updated funcExpression");
		}
		funcExpression();

		const constantFunc = function(){
			console.log("Initialized with const!")
		};
		constantFunc();//

	//Function Scoping
		/*
			Scope-accessiblity/visibility of variables
			JS Variables has 3 types of scope:
				1. local/block scope - within a block
				2. global scope - within the document
				3. function scope - can only be used inside function
		*/

					/*block scope
					{
						let a = 1;
					}
					let a = 1;

					//global scope
					let a = 1;

					//function scope
					function sample(){
						let a = 1;
					}*/

			{
				let localVar = "Armando Perez";
			}

			let globalVar = "Mr. Worldwide";
			console.log(globalVar);
			//console.log(localVar);//this will result in an error since it's not inside the block

			function showNames(){
				var functionVar = "Joe";
				const functionConst = "John";
				let functionLet = "Jane";

				console.log(functionVar);
				console.log(functionConst);
				console.log(functionLet);
			}
			/*console.log(functionVar);
			console.log(functionConst);
			console.log(functionLet);*/ //this will result an error, outside function block, need to invoke 

			showNames();

	//Nested Functions
			//function within a function

			function myNewFunction(){
				let name = "Jane";
					function nestedFunction(){
						let nestedName = "John";
						console.log(name);
					}
					//console.log(nestedName); -- this will be an error since outside function scope
					nestedFunction();
			}
			myNewFunction();//result: jane

	//Function and Global Scoped Variables
			//Global Scoped Variables
			let globalName = "Cardo";
			
			function myNewFunction2(){
				let nameInside = "Hillary"
				console.log(globalName);
			}
			myNewFunction2();
			//console.log(nameInside); -- not a global function

				//Using alert ()
				//alert("This will run immediately when the page loads.") -- works
				/* function showSampleAlert(){
					alert("Hello, Earthlings! This is from a function!")
				}
				showSampleAlert(); //--also works

				console.log("I will only log when the alert is dismissed!")

				//Using prompt ()
				let samplePrompt = prompt("Enter your Name: ");
				console.log("Hi, I am " + samplePrompt); //this will ask your name and your name will show in the console log, no empty will return empty string, cancel will return null

				function printWelcomeMessage(){
						let firstName = prompt("Enter first name: ")
						let lastName = prompt("Enter last name: ")

						console.log("Hello, " + firstName + " " + lastName + "!");
						console.log("Welcome to my page!")
				}

				printWelcomeMessage(); */

	//The Return Statement
			// allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
			// can return numbers, arrays, objects, texts

			function returnFullName(){
				return "Jeffrey" + ' ' + "Smith" + ' ' + "Bezos";
				console.log("This message will not be printed!");
			}
			let fullName = returnFullName();
			console.log(fullName); // example 1

			function returnFullAddress(){
				let fullAddress = {
					street: "#44 Maharlika St",
					city: "Cainta",
					province: "Rizal"
				};
				return fullAddress;
			}
			let myAddress = returnFullAddress();
			console.log(myAddress); //example 2

			function printPlayerInfo(){
				console.log("Username: " + "dark_magician");
				console.log("Level: " + 95);
				console.log("Job: " + "Mage");

			}

			let user1 = printPlayerInfo();
			console.log(user1); //example 3 - will show undefined due to value not saved from absence of return function

			function returnSumOf5And10(){
				return 5 + 10; //if return is replaced as console.log, results will not show
			}
			let sumOf5And10 = returnSumOf5And10();//result 15
			console.log(sumOf5And10);

			let total = 100 + returnSumOf5And10();//result 100
			console.log(total);//example 4

			function getGuildMembers(){
				return["Lulu","Tristana","Teemo"];
			}
			console.log(getGuildMembers());

	//Function Naming Conventions

	function getCourses (){
		let courses = ["ReactJs","ExpressJs101","MongoDB 101"]
		return courses;
	}
	let courses = getCourses();//value inside getcourses function is assigned to courses variable
	console.log(courses);

	//avoid generic/pointless/inappropriate names for functions that don't demo purpose of function
	function pikachu (){
		let color = "pink";
		return name;
	}

	//use camelCase
	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
	}
	displayCarInfo();



	//-----ACTIVITY HEADS UP 
	// Item 1 on activity
	function getUserInfo(){
		return {
			name: "John Doe",
			age: 25,
			address: "123 Street. Quezon City",
			isMarried: false,
			petName: "Danny"
		}
	}
	let userInfo: getUserInfo();
	console.log(userInfo);

















