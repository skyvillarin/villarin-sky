console.log("Array Non-Mutator and Iterator Methods!");
// Non mutator
	// do not modify or change an array after they are created
	// do not manipulate the orginal array performing various tasks such as:
		//returning elements from an array
		//combining arrays
		//printing the output

	let countries = ['US','PH','CAN','SG','TH','PH','FR','DE'];
	console.log(countries);

	// indexOf()
		// returns the index number of the FIRST matching element found in an array, scans from first to last element
		// if not match found = result will be -1

			let firstIndex = countries.indexOf('PH');
			console.log('Result of indexOf method: ' + firstIndex); // result 1
			let invalidCountry = countries.indexOf('BR');
			console.log('Result of indexOf method: ' + invalidCountry); //result -1, not 0 because 0 is the first element
		
	//lastIndexOf()
		// returns the index number of the LAST matching element found in an array, scans from last to first

			let lastIndex = countries.lastIndexOf('PH');
			console.log('Result of lastIndexOf method: ' + lastIndex); //result 5
			let lastIndexStart = countries.lastIndexOf('PH', 3);
			console.log('Result of lastIndexOf method: ' + lastIndexStart); //result 1 since we dictated the index number on where to start scanning from last to first

	//slice()
		// portions or slices elements from an array and returns a new array
			
			let slicedArrayA = countries.slice(2);
			console.log('Result of slice method: ' + slicedArrayA); // CAN,SG,TH,PH,FR,DE
			let slicedArrayB = countries.slice(2,4);
			console.log('Result of slice method: ' + slicedArrayB); // CAN,SG, 4 is the end slice
			let slicedArrayC = countries.slice(-3);
			console.log('Result of slice method: ' + slicedArrayC); // PH,FR,DE since negative number means start from the last 

	//toString()
		// returns an array as a string separated by commas
			let stringArray = countries.toString();
			console.log('Result of toString method: ' + stringArray);
			// result US,PH,CAN,SG,TH,PH,FR,DE

			let sampArr = [1,2,3];
			console.log(sampArr.toString()); // result 123


	//concat();
		// combines two arrays and returns the combined result
			let taskArrayA = ['drink html','eat javascript'];
			let taskArrayB = ['inhale css','breath mongoDB'];
			let taskArrayC = ['get git','be node'];

			let tasks = taskArrayA.concat(taskArrayB);
			console.log('Result from concat method:');
			console.log(tasks);
			// ['drink html', 'eat javascript', 'inhale css', 'breath mongoDB']

			let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
			console.log('Result from concat method:');
			console.log(allTasks);
			// ['drink html', 'eat javascript', 'inhale css', 'breath mongoDB', 'get git', 'be node']

			let combinedTasks = taskArrayA.concat('smell express', 'throw react');
			console.log('Result from concat method:');
			console.log(combinedTasks);
			// ['drink html', 'eat javascript', 'smell express', 'throw react']

	//join();
		// returns an array as string separated by specified separator (string data type)
			let users = ['John','Jane','Joe','James']; 
			console.log(users.join()); // John,Jane,Joe,James
			console.log(users.join('')); // JohnJaneJoeJames
			console.log(users.join(' - ')); // John - Jane - Joe - James

// Iterator Methods
	// these are loops designed to perform repetitive tasks on arrays 
		// Array iteration methods normally work with a function supplied as an argument

	//forEach()
		// similar to a for loop that iterates on each array element
		// for each item in the array, the ANONYMOUS FUNCTION (no name) passed in the forEach() method will be run 
		// the anonymous function is able to receive the current item being iterated or loop over by assigning a PARAMETER
		// variable names for arrays are normally written in the plural form but it is a common practice to use singular form of the array content for the parameter names used in array looops

			console.log(allTasks);
				allTasks.forEach(function(task){
					console.log(task);
				}); 
				// task is the singular form parameter of allTasks
				// result
					/* drink html
						eat javascript
						inhale css
						breath mongoDB
						get git
						be node
					*/

				let filteredTasks = [];
					allTasks.forEach(function(task){
						if(task.length>10){
							filteredTasks.push(task);
						}
					}); 
				console.log('Result of filteredTasks: ');
				console.log(filteredTasks);
				// result ['eat javascript', 'breathe mongoDB'] - since these are the only characters greater than 10

	//map ()
		// iterates on each element AND returns new array with different values depending on the result of the function's operation
			// we require the use of a "return"

			let numbers = [1,2,3,4,5];
			let numberMap = numbers.map(function(number){
				return number * number;
			})
			console.log('Original Array');
			console.log(numbers);
			console.log('Result of map method: ')
			console.log(numberMap); // result [1, 4, 9, 16, 25]

	//map vs forEach
			let numberForEach = numbers.forEach(function(number){
				return number * number;
			})
			console.log(numberForEach) // result undefined since forEach does not return anything

	//every()
		// checks if ALL elements in an array meet a given condition
		// returns a boolean value

			let allValid = numbers.every(function(number){
				return (number < 3)
			})
			console.log('Result of every method: ');
			console.log(allValid); // false since not all numbers less than 3

	//some()
		// checks if AT LEAST ONE element in the array meets the given condtion

			let someValid = numbers.some(function(number){
				return (number < 2);
			})
			console.log('Result of some method: ');
			console.log(someValid); // true 

	//filter()		
		// returns a new array that contains elements which meets the given condition
		// returns an empty array if no elements were found

			let filterValid = numbers.filter(function(number){
				return(number < 3);
			})
			console.log('Result of filter method: ');
			console.log(filterValid); // result [1, 2]

			let nothingFound = numbers.filter(function(number){
				return(number = 0);
			})
			console.log('Result of filter method: ');
			console.log(nothingFound); // result []

			// MINI ACTIVITY
					//Filter out numbers from the numbers array that are less than 3 using forEach.
						let filteredNumbers = []
						numbers.forEach(function(number){
							if (number < 3){
								filteredNumbers.push(number);
							}
						})
						console.log("Result of mini activity for each:")
						console.log(filteredNumbers);
						// result [1, 2]
	
	// includes()
			//checks if the argument passed can be found in the array

				let products = ['Mouse','Keyboard','Laptop','Monitor'];
				let productFound1 = products.includes('Mouse')
				console.log(productFound1); // true
				let productFound2 = products.includes('Headset')
				console.log(productFound2); // false

			// Methods can be "chained" using them one after another
					//methods in a method
						let filteredProducts = products.filter(function(product){
							return product.toLowerCase().includes('a');
						})
						console.log(filteredProducts);
						// result ['Keyboard', 'Laptop']

	// reduce()
			// evaluates elements from left to right and returns/reduces the array into a single value
				// accumulator - parameter in the function stores the result for every iteration of the loop
				// current value - parameter is the current element in the array that is evaluated in each iteration of the loop

				/*
					PROCESS:
						1. First element in the array is stored in the "accumulator"
						2. Second element in the array is stored in the "current value"
						3. An operation is performed on the 2 elements
						4. The loop repeats the whole step 1-3 until all elements have been worked on
				*/
				console.log(numbers); // [1, 2, 3, 4, 5]
											// 1(accumulator) + 2 (current value)= 3 
											// 3 (now accumulator) + 3 (3rd element/current value) = 6 
											// 6 (now accumulator) + 4 (4th element/current value) = 10 
											// 10 + 5 = 15 

				let iteration = 0
				let reducedArray = numbers.reduce(function(x,y){
					console.warn('Current iteration ' +  ++iteration); //increases iteration by 1);
					console.log('accumulator: ' + x);
					console.log('current value: ' + y);
					return x + y
				})
				console.log('Result of reduce method: ' + reducedArray); // 15 result

			// can reduce strings
				let list = ['Hello ','From ','The ','Other ','Side '];
				let reducedJoin = list.reduce(function(x,y){
					console.log(x);
					console.log(y)
					return x + ' ' + y
				})
				console.log("Result of reducing strings: " + reducedJoin); // result Hello  From  The  Other  Side 










