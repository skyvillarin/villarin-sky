import { useState } from 'react';

export default function EditProfile({ user, onUpdate }) {
  const [firstName, setFirstName] = useState(user.firstName);
  const [lastName, setLastName] = useState(user.lastName);
  const [mobileNo, setMobileNo] = useState(user.mobileNo);

  const handleUpdate = () => {
    // Create a payload with the updated user data
    const updatedUser = {
      firstName,
      lastName,
      mobileNo,
    };

    // Send a PUT request to update the user's profile
    fetch('http://localhost:4000/users/profile', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(updatedUser),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then((data) => {
        console.log(data);
        onUpdate(data); // Pass the updated user data to the parent component
      })
      .catch((error) => {
        console.error('Error updating user profile:', error);
      });
  };

  return (
    <div className="container mt-5" >
      <h3>Edit Profile</h3>
      <form>
      <div className="mb-3">
        <label className="form-label">First Name:</label>
        <input
          type="text"
          className="form-control"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
      </div>
      <div className="mb-3">
        <label className="form-label">Last Name:</label>
        <input
          type="text"
          className="form-control"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
      </div>
      <div>
        <label className="form-label">Mobile No:</label>
        <input
          type="text"
          className="form-control"
          value={mobileNo}
          onChange={(e) => setMobileNo(e.target.value)}
        />
      </div>
      <button className='btn btn-primary mt-3' onClick={handleUpdate}>Update Profile</button>
      </form>
    </div>
  );
}
