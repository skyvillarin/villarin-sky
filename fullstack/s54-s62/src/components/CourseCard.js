import { useState } from 'react' //purpose of useState (which is a hook) is to create a new state
import { Card, Button } from "react-bootstrap";
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';

// export default function CourseCard() {
//   return (
//     <Card>
//       <Card.Body className="mt-3 mb-3">
//         <Card className="cardHighlight p-3">
//           <Card.Title>Sample Course</Card.Title>
//           	<Card.Subtitle>Description:</Card.Subtitle>
//           		<Card.Text>This is a sample course offering.</Card.Text>
//          	 	<Card.Subtitle>Price:</Card.Subtitle>
//          			<Card.Text>PhP 40,000</Card.Text>
//           <row>
//             <Button variant="primary">Enroll</Button>
//           </row>
//         </Card>
//     </Card.Body>
//   );
// }

//ACTIVITY OUTPUT
export default function CourseCard({courseProp}) {

  // console.log(props);
  // console.log(typeof props);

const {_id, name, description, price} = courseProp;
console.log(useState(0))

// Syntax
  // const [getter, setter] = useState(initialGetterValue)
// const [count, setCount] = useState(0);

// console.log(useState(0));

// function enroll() {
//   setCount(count + 1);
//   console.log('Enrollees: ' + count);
// }



//--------ACTIVITY: 
/*

Create a seats state in the CourseCard component and set the initial value to 30.
For every enrollment, deduct one to the seats.
If the seats reaches zero:    
- Do not add to the count.    
- Do not deduct to the seats.
Show an alert that says "no more seats".

*/

//const [count, setCount] = useState(0);
//const [seats, setSeats] = useState(30);

  // function enroll() {
  //   if (seats > 0) {
  //     setCount(count + 1);
  //     setSeats(seats - 1);
  //   } else {
  //     alert("No more seats available.");
  //   }
  // }
//-----------

  return (
    <Card className="mt-3">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
      </Card.Body>
    </Card>
  )
}


// Check if the CourseCard component is getting the correct prop types
CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}