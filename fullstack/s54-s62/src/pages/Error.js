// import Banner from "../components/Banner";
// import { Container } from "react-bootstrap";
// import { Link } from "react-router-dom";

// export default function Error() {
//   const linkStyle = {
//     color: "white",
//     textDecoration: "none", // Remove underline
//   };
//   return (
//     <Container>
//       <Banner
//         title="404 - Not Found"
//         subtitle="The page you are looking for cannot be found."
//         button={
//           <Link to="/" style={linkStyle}>
//             Back Home
//           </Link>
//         }
//       />
//     </Container>
//   );
// }


import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }
    
    return (
        <Banner data={data}/>
    )
}